###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "COPYING".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
cmake_minimum_required(VERSION 3.15)


# Define HepMC3 external project
include(ExternalProject)
message(STATUS "HepMC3 will use tag \"${HEPMC3_GIT_TAG}\" from repository ${HEPMC3_GIT_SOURCE}")

ExternalProject_Add(HepMC3Ext
  GIT_REPOSITORY ${HEPMC3_GIT_SOURCE}
  GIT_TAG ${HEPMC3_GIT_TAG}
  CMAKE_ARGS 
  -DCMAKE_INSTALL_PREFIX=${CMAKE_SOURCE_DIR}/InstallArea/$ENV{BINARY_TAG}
       -DHEPMC3_ENABLE_PYTHON=OFF
       -DHEPMC3_ENABLE_ROOTIO=OFF # FIXME: disabled because of HepMC & HepMC3 namespace collision
       -DHEPMC3_INSTALL_INTERFACES=ON
       # Do not install examples (installed directories are detected as
       # packages by the old Gaudi toolchain):
       -DHEPMC3_INSTALL_EXAMPLES=OFF
       -DCMAKE_BUILD_TYPE=Release
       #-DROOT_DIR=${ROOTSYS}
       -DCMAKE_INSTALL_LIBDIR=lib
       -DCMAKE_CXX_FLAGS_DEBUG=${CMAKE_CXX_FLAGS_DEBUG}
       -DCMAKE_C_FLAGS_DEBUG=${CMAKE_C_FLAGS_DEBUG}
       -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
       -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
)
