# Changelog
All notable changes to this project will be documented in this file.

Project Coordinators: Gloria Corti @gcorti, Michal Mazurek @mimazure 

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [v1r0](https://gitlab.cern.ch/gaussino/GaussinoExtLib/-/releases/v1r0) - 2024-02-06
This first release includes all code needed for a first release of Gaussino customised for the 
LHCb experiment. Notably:

- It fixes the version of HepMC3 to a local modified copy based on [3.2.2](https://gitlab.cern.ch/hepmc/HepMC3/-/releases/3.2.2) with two [patches](https://gitlab.cern.ch/Gaussino/HepMC3/-/tree/official_patched?ref_type=heads) applied on top of it
- It fixes the version of DDG4 to a local mirror based on DD4Hep [v01-26](https://github.com/AIDASoft/DD4hep/releases/tag/v01-26) with [patches](https://gitlab.cern.ch/Gaussino/DD4hep/-/commits/v01-26-patches/?ref_type=heads)
- It fixes the version of Geant4 to the LHCb build [v10r7p3t1](https://gitlab.cern.ch/lhcb/Geant4/-/releases/v10r7p3t1) 
- It fixes the version of LHCb to [v55r0](https://gitlab.cern.ch/lhcb/LHCb/-/releases/v55r0)

It should be noted that at the moment Gaussino still depends on a few LHCb packages, hence it needs to be built againt the core LHCb project. Work is in progress to remove this dependecy. Implicitley this also drives the Gaussino dependency from Gaudi. 

### Changed
- Fix required version of Geant4 and LHCb needed for first release (Gaussino/gaussinoextlibs!18)
- Introduce dd4hep v01-26 to be used when LCG 104 is used (Gaussino/gaussinoextlibs!16)
- Use Geant4 10.7.3 (Gaussino/gaussinoextlibs!10)
