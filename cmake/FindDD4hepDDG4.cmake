###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb and FCC Collaborations  #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "COPYING".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Locate DD4hepDDG4 library
# Defines:
#  Variables:
#   DD4hepDDG4_FOUND
#   DD4hepDDG4_INCLUDE_DIRS
#   DD4hepDDG4_LIBRARIES
#  Targets:
#   DD4hep::DDPython
#   DD4hep::DDG4Python
#   DD4hep::DDG4LCIO
#   DD4hep::DDG4
#   DD4hep::DDG4Plugins

include(${GAUSSINOEXTLIBS_PREFIX_DIR}/cmake/DD4hep.cmake)
DD4HEP_SETUP_GEANT4_TARGETS()

include(${GAUSSINOEXTLIBS_PREFIX_DIR}/cmake/DD4hepDDG4Config-targets.cmake)
get_target_property(DD4hepDDG4_INCLUDE_DIRS DD4hep::DDG4 INTERFACE_INCLUDE_DIRECTORIES)
set(DD4hepDDG4_LIBRARIES DD4hep::DDG4)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    DD4hepDDG4
    DEFAULT_MSG
    DD4hepDDG4_INCLUDE_DIRS
    DD4hepDDG4_LIBRARIES
)
