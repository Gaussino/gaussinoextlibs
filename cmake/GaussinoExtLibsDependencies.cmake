if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()

# -- Public dependencies
lhcb_find_package(LHCb 55.0 REQUIRED)
lhcb_find_package(Geant4 10.7.3.1 REQUIRED)
if(NOT GEANT4_PROJECT_ROOT AND Geant4_DIR MATCHES "^(.*)/InstallArea/")
    set(GEANT4_PROJECT_ROOT "${CMAKE_MATCH_1}")
endif()

# -- Private dependencies
if(WITH_GaussinoExtLibs_PRIVATE_DEPENDENCIES)
    if(USE_DD4HEP)
        set(CMAKE_CXX_STANDARD ${GAUDI_CXX_STANDARD})
        find_package(DD4hep REQUIRED DDCore)
    endif()
endif()
