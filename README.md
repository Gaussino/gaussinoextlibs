# GaussinoExtLibs

The scope of this software is as an helper to 
Gaussino to allow the build of external libraries, in particular HepMC and DDG4, 
consistently with what used by other experiments' software.  

It provides a thin cmake layer to define the repository where sources are located and version to be used therein.

It allows to ensure the build on platforms as needed by users and select version of dependent packages, e.g. DDG4 compilation against a chosen version of Geant4.

Each user may require its own customizstion of this package.